"""
Reading html file and returning cleaned data
Author: William Arias
V1.0
"""
import argparse
import json
import logging
import os
from bs4 import BeautifulSoup


logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def clean_data(args):
    """ This functions takes args as inputs and convert the HTML into raw text of string type  """
    with open(args.input_dataset) as page:
        soup = BeautifulSoup(page, "html.parser")
        text = soup.get_text()
        text = " ".join(text.split())
        logger.info("SUCCESS: Converted the input website to string data")
        return text


def write_data(text, args):
    a_dict = {'context': text}
    # mlflow adjustment
    data_path = "data/processed_data/"+args.output_artifact
    os.makedirs(os.path.dirname(data_path), exist_ok=True)
    with open(data_path, 'w') as output_file:
        json.dump(a_dict, output_file, indent=2)
        logger.info("SUCCESS: Stored text data as JSON in Filesystem")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="step to clean data")

    parser.add_argument(
        "--input_dataset",
        type=str,
        help="input artifact obtained from scrapping the web",
        required=True
    )

    parser.add_argument(
        "--output_artifact",
        type=str,
        help="output name for cleaned data artifact provided as input",
        required=True
    )

    args = parser.parse_args()

    text = clean_data(args)
    write_data(text, args)
