
import pytest
import json
import os


@pytest.fixture(scope="session")
def context_data():
    # OS env for CI
    # Change path testing locally
    local_path = os.environ['ARTIFACT']
    with open(local_path, 'r') as output_data:
        json_data = json.load(output_data)
        return json_data['context']


def test_context_length(context_data):
    """
       We test that we have enough data to provide context to the Q&A Pipeline
    """

    assert len(context_data) > 1000, "not enough words to provide context"


